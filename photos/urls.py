from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from photos import views

router = DefaultRouter()
router.register(r'photos', views.PhotoViewSet)

app_name = 'photos'
urlpatterns = [
    url(r'^', include(router.urls))
]
