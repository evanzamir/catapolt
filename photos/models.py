from uuid import uuid4

from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.core.files.storage import default_storage
from taggit.managers import TaggableManager
from taggit.models import GenericUUIDTaggedItemBase, TaggedItemBase
from versatileimagefield.fields import VersatileImageField

from scouting.core.models import TimestampModerated
from profiles.models import Profile
from scouting.settings import AUTH_USER_MODEL


def user_directory_path(instance, filename):
    return 'img/u/{}/{}'.format(instance.profile.id,
                                filename)


class UUIDTaggedItem(GenericUUIDTaggedItemBase, TaggedItemBase):
    class Meta:
        verbose_name = _("Tag")
        verbose_name_plural = _("Tags")


class Photo(TimestampModerated):
    profile = models.ForeignKey(AUTH_USER_MODEL, related_name='profiles', on_delete=models.CASCADE, null=True)
    id = models.UUIDField(default=uuid4, editable=False, primary_key=True, db_index=True)
    slug = models.SlugField(max_length=80, editable=False)
    title = models.TextField(blank=True, null=True)
    description = models.TextField(verbose_name='description of entity', blank=True, null=True)
    photo = VersatileImageField(upload_to=user_directory_path, height_field="height", width_field="width", blank=True)
    height = models.IntegerField(blank=True)
    width = models.IntegerField(blank=True)
    tags = TaggableManager(blank=True, through=UUIDTaggedItem)
    hash = models.CharField(max_length=64, unique=True, null=True)
    size = models.BigIntegerField('size of file in bytes', blank=True, null=True)
    likes = models.ManyToManyField(AUTH_USER_MODEL, blank=True)
    reports = models.ManyToManyField(AUTH_USER_MODEL, blank=True, related_name='reports')
    wins = models.ManyToManyField('self', blank=True, symmetrical=False, related_name='related_wins')
    losses = models.ManyToManyField('self', blank=True, symmetrical=False, related_name='related_losses')
    win_pct = models.FloatField(default=-1, blank=True, null=True)

    class Meta:
        verbose_name_plural = "photos"

    def __str__(self):
        return self.title

    def delete(self, using=None, keep_parents=False):
        default_storage.delete("{}".format(self.photo))
        super().delete()

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Photo, self).save(*args, **kwargs)

    def num_wins(self):
        return self.wins.count()

    def num_losses(self):
        return self.losses.count()

    def num_comments(self):
        return self.photo_comments.count()

    def num_reports(self):
        return self.reports.count()


class PhotoReport(TimestampModerated):
    STATUS_CHOICES = (
        (1, "User reported content"),
        (2, "Other inappropriate content"),
        (3, "Adult content"),
        (4, "Hateful and/or abusive content"),
        (5, "Violent content"),
        (6, "Spam or other commercial content"),
        (7, "Copyright infringement"),
        (8, "Incorrect tags"),
    )
    id = models.UUIDField(default=uuid4, editable=False, primary_key=True)
    profile = models.ForeignKey(AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    photo = models.ForeignKey(Photo, on_delete=models.CASCADE, null=True)
    status = models.PositiveSmallIntegerField(choices=STATUS_CHOICES, default=1)

    class Meta:
        verbose_name_plural = "photo_reports"
