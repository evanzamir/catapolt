from django.contrib import admin
from django.core.files.storage import default_storage

from photos.models import Photo


# Register your models here.

class PhotoAdmin(admin.ModelAdmin):
    model = Photo
    list_display = ['id', 'photo', 'height', 'width', 'title', 'updated', 'created',
                    'moderation_code', 'tag_list', 'hash', 'size']
    actions = ['delete_model']

    def delete_model(self, request, queryset):
        for obj in queryset:
            obj.delete()

    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related('tags')

    def tag_list(self, obj):
        return u", ".join(o.name for o in obj.tags.all())


admin.site.register(Photo, PhotoAdmin)
