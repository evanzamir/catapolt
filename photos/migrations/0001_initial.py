# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-12-05 04:54
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import photos.models
import taggit.managers
import uuid
import versatileimagefield.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('contenttypes', '0002_remove_content_type_name'),
        ('taggit', '0002_auto_20150616_2121'),
    ]

    operations = [
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='datetime object first created')),
                ('moderation_code', models.PositiveSmallIntegerField(default=0, verbose_name='moderation status code')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='datetime object updated')),
                ('id', models.UUIDField(db_index=True, default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('slug', models.SlugField(editable=False, max_length=80)),
                ('title', models.TextField(blank=True, null=True)),
                ('description', models.TextField(blank=True, null=True, verbose_name='description of entity')),
                ('photo', versatileimagefield.fields.VersatileImageField(blank=True, height_field='height', upload_to=photos.models.user_directory_path, width_field='width')),
                ('height', models.IntegerField(blank=True)),
                ('width', models.IntegerField(blank=True)),
                ('hash', models.CharField(max_length=64, null=True, unique=True)),
                ('size', models.BigIntegerField(blank=True, null=True, verbose_name='size of file in bytes')),
                ('win_pct', models.FloatField(blank=True, default=-1, null=True)),
                ('likes', models.ManyToManyField(blank=True, to=settings.AUTH_USER_MODEL)),
                ('losses', models.ManyToManyField(blank=True, related_name='related_losses', to='photos.Photo')),
                ('profile', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='profiles', to=settings.AUTH_USER_MODEL)),
                ('reports', models.ManyToManyField(blank=True, related_name='reports', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'photos',
            },
        ),
        migrations.CreateModel(
            name='PhotoReport',
            fields=[
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='datetime object first created')),
                ('moderation_code', models.PositiveSmallIntegerField(default=0, verbose_name='moderation status code')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='datetime object updated')),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('status', models.PositiveSmallIntegerField(choices=[(1, 'User reported content'), (2, 'Other inappropriate content'), (3, 'Adult content'), (4, 'Hateful and/or abusive content'), (5, 'Violent content'), (6, 'Spam or other commercial content'), (7, 'Copyright infringement'), (8, 'Incorrect tags')], default=1)),
                ('photo', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='photos.Photo')),
                ('profile', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'photo_reports',
            },
        ),
        migrations.CreateModel(
            name='UUIDTaggedItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('object_id', models.UUIDField(db_index=True, verbose_name='Object id')),
                ('content_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='photos_uuidtaggeditem_tagged_items', to='contenttypes.ContentType', verbose_name='Content type')),
                ('tag', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='photos_uuidtaggeditem_items', to='taggit.Tag')),
            ],
            options={
                'verbose_name': 'Tag',
                'verbose_name_plural': 'Tags',
            },
        ),
        migrations.AddField(
            model_name='photo',
            name='tags',
            field=taggit.managers.TaggableManager(blank=True, help_text='A comma-separated list of tags.', through='photos.UUIDTaggedItem', to='taggit.Tag', verbose_name='Tags'),
        ),
        migrations.AddField(
            model_name='photo',
            name='wins',
            field=models.ManyToManyField(blank=True, related_name='related_wins', to='photos.Photo'),
        ),
    ]
