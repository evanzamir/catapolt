import json

from django.db.models import Count, F

from rest_framework import permissions, viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework.decorators import list_route, detail_route
from taggit.models import Tag

from photos.models import Photo, PhotoReport
from photos.serializers import PhotoSerializer
from comments.serializers import PhotoCommentSerializer
from comments.models import PhotoComment
from scouting.permissions import IsOwnerOrReadOnly


# Create your views here.
class PhotoViewSet(viewsets.ModelViewSet):
    queryset = Photo.objects.all()
    serializer_class = PhotoSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsOwnerOrReadOnly, permissions.AllowAny,)
    parser_classes = (MultiPartParser, FormParser)

    def perform_create(self, serializer):
        serializer.save(profile=self.request.user,
                        likes=[],
                        wins=[],
                        losses=[])

    def list(self, request, *args, **kwargs):
        photos = Photo.objects.filter(profile=self.request.user).order_by('-created')
        serializer = self.get_serializer(photos, many=True)
        return Response(
            data=serializer.data
        )

    @list_route(methods=['get'])
    def community(self, request):
        """

        :param request:
        :return:
        """
        # tags = json.loads(request.data['tags'])
        tags = json.loads(request.query_params.get('tags'))
        sort_type = request.query_params.get('sortType')
        photos = Photo.objects.all().order_by("-{}".format(sort_type))
        for tag in tags:
            photos = photos.filter(tags__name=tag)
        photos = photos.distinct()
        page = self.paginate_queryset(photos)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(photos, many=True)
        return Response(
            data=serializer.data
        )

    @list_route(methods=['get'])
    def my_tags(self, request):
        """
        get list of tags to search
        :param request:
        :return:
        """
        tags = Tag.objects.filter(photo__owner=self.request.user).values_list('name', flat=True).distinct().order_by(
            'name')
        return Response(
            data=tags
        )

    @list_route(methods=['get'])
    def tag_list(self, request):
        """

        :param request:
        :return:
        """
        tags = Tag.objects.values_list('name', flat=True).distinct().order_by(
            'name')
        return Response(
            data=tags
        )

    @detail_route(methods=['post'])
    def like(self, request, pk):
        """

        :param request:
        :return:
        """
        photo = Photo.objects.get(id=pk)
        if self.request.user in photo.likes.all():
            photo.likes.remove(self.request.user)
        else:
            photo.likes.add(self.request.user)
        return Response(
            data=self.get_serializer(photo).data
        )

    @detail_route(methods=['get'])
    def comments(self, request, pk):
        """

        :param request:
        :param pk:
        :return:
        """
        comments = PhotoComment.objects.filter(photo__id=pk).order_by('created')
        return Response(
            data=PhotoCommentSerializer(comments, many=True).data
        )

    @detail_route(methods=['post'])
    def report(self, request, pk):
        """

        :param request:
        :param pk:
        :return:
        """
        photo = Photo.objects.get(id=pk)
        PhotoReport.objects.create(
            profile=self.request.user,
            photo=photo,
            status=request.data['status']
        )
        photo.reports.add(self.request.user)
        return Response(
            data=self.get_serializer(photo).data
        )