import base64
import six
import uuid
import imghdr
import datetime
import io
import math

from django.core.files.base import ContentFile
from django.core.files.images import ImageFile

from PIL import Image, ImageFilter, ImageEnhance
from resizeimage import resizeimage
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from google.cloud.storage import Blob
from google.cloud import storage
from taggit_serializer.serializers import TagListSerializerField, TaggitSerializer
from versatileimagefield.serializers import VersatileImageFieldSerializer
from versatileimagefield.fields import VersatileImageField

from photos.models import Photo
from profiles.models import Profile
from profiles.serializers import ProfileSerializer
from scouting.config.base import GS_BUCKET_NAME

client = storage.Client()
bucket = client.get_bucket(GS_BUCKET_NAME)


class Base64ImageField(serializers.ImageField):
    """
    A Django REST framework field for handling image-uploads through raw post data.
    It uses base64 for encoding and decoding the contents of the file.

    Heavily based on
    https://github.com/tomchristie/django-rest-framework/pull/1268

    Updated for Django REST framework 3.
    """

    MEGAPIXEL_LIMIT = 2000000

    def to_internal_value(self, data):
        file_name = str(uuid.uuid4())
        # Get the file name extension:
        file_extension = self.get_file_extension(file_name, data)

        complete_file_name = "{}.{}".format(file_name, file_extension)
        uploaded = data.read()
        img = Image.open(io.BytesIO(uploaded))
        new_image_io = io.BytesIO()
        megapixels = img.width * img.height

        # reduce size if image is bigger than MEGAPIXEL_LIMIT
        if megapixels > self.MEGAPIXEL_LIMIT:
            resize_factor = math.sqrt(megapixels/self.MEGAPIXEL_LIMIT)
            resized = resizeimage.resize_thumbnail(img, [img.width/resize_factor,
                                                         img.height/resize_factor])
            resized.save(new_image_io, format=file_extension.upper())
        else:
            img.save(new_image_io, format=file_extension.upper())

        content = ContentFile(new_image_io.getvalue(), name=complete_file_name)
        return super(Base64ImageField, self).to_internal_value(content)

    def get_file_extension(self, file_name, decoded_file):
        if decoded_file.content_type == 'image/jpeg':
            extension = "jpeg"
        elif decoded_file.content_type == 'image/png':
            extension = "png"
        else:
            extension = decoded_file.content_type

        return extension

    def to_representation(self, value):
        try:
            blob = Blob(name=value.name, bucket=bucket)
            # signed_url = blob.generate_signed_url(expiration=datetime.timedelta(hours=24))
            # blob.make_public()
            # $ gsutil defacl set public-read gs://bucket #make all objects public by default
            return blob.public_url
        except ValueError as e:
            return value


class PhotoSerializer(serializers.ModelSerializer, TaggitSerializer):
    profile = ProfileSerializer(read_only=True)
    tags = TagListSerializerField()
    photo = Base64ImageField(use_url=True)

    class Meta:
        model = Photo
        fields = ('photo', 'height', 'width', 'slug', 'title', 'id', 'created', 'updated',
                  'moderation_code', 'tags', 'hash', 'description', 'size', 'likes', 'wins', 'losses', 'reports',
                  'num_wins', 'num_losses', 'win_pct', 'profile', 'num_comments', 'num_reports')

    def create(self, validated_data):
        validated_data.pop('likes')
        validated_data.pop('reports')
        validated_data.pop('wins')
        validated_data.pop('losses')
        profile = validated_data.pop('profile')
        if profile.credits < 1:
            raise ValidationError
        profile.credits -= 1
        profile.save()
        photo = Photo.objects.create(profile=profile,
                                     **validated_data)
        p = Photo.objects.get(id=photo.id)
        [p.tags.add(tag) for tag in validated_data['tags']]
        return photo
