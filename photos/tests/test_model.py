from django.contrib.auth import models
from django.core.files.storage import default_storage
from django.core.files.images import ImageFile

from rest_framework import test

from photos.models import Photo


class PhotoTest(test.APITestCase):
    def setUp(self):
        self.user = models.User.objects.create(username='evanzamir', password='georgina2011')
        # self.test_user = models.User.objects.create(username='thecity2', password='judah2016')

    def test_create_and_delete_photo(self):
        test_path = 'img/u/{}/{}'
        photo = Photo(owner=self.user, title='Awesome Juancho', slug='awesome-juancho')
        photo.photo.save('test', ImageFile(open('public/jhg1.jpg', 'rb')))
        self.assertTrue(default_storage.exists(test_path.format(self.user.id, photo.uuid)))
        ph = Photo.objects.get(photo=photo.photo)
        ph.delete()
        self.assertFalse(default_storage.exists(test_path.format(self.user.id, photo.uuid)))
        with self.assertRaises(Photo.DoesNotExist):
            Photo.objects.get(photo=photo.photo)
