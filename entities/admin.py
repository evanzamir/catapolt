from django.contrib import admin

from entities.models import Entity


# Register your models here.

class EntityAdmin(admin.ModelAdmin):
    model = Entity
    list_display = ['name', 'description', 'media_url', 'created', 'updated']


admin.site.register(Entity, EntityAdmin)
