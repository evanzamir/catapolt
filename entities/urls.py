from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from entities import views

router = DefaultRouter()
router.register(r'entities', views.EntityViewSet)
router.register(r'entity-likes', views.EntityLikeViewSet)
# router.register(r'users', views.UserViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
]
