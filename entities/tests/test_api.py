from django.contrib.auth import models
from rest_framework import test, status
from rest_framework.test import APIClient

from entities.models import Entity, EntityLike


class EntitiesAPITests(test.APITestCase):
    """
    Testing django-rest-framework API
    """

    def setUp(self):
        self.user = models.User.objects.create(username='evanzamir', password='georgina2011')
        self.test_user = models.User.objects.create(username='thecity2', password='judah2016')
        self.test_user2 = models.User.objects.create(username='markjackson', password='ellie2016')

        steph = Entity(name='Stephen Curry',
                       slug='stephen-curry',
                       description='Point guard for the Warriors',
                       media_url='http://www.images.com/StephenCurry.jpg',
                       owner=self.user)

        steph.save()

        steph.tags.add('point_guard', 'mvp', '3pt', 'warriors')

        kd = Entity(name='Kevin Durant',
                    slug='kevin-durant',
                    description='small forward for the Warriors',
                    media_url='http://www.images.com/KevinDurant.jpg',
                    owner=self.user)

        kd.save()
        kd.tags.add('small_forward', 'mvp', 'warriors')

        iggy = Entity(name='Andre Iguodala',
                      slug='andre-iguodala',
                      description="2015 NBA Finals MVP; does not like to be called Iggy",
                      media_url='http://www.images.com/AndreIguodala.jpg',
                      owner=self.test_user)

        iggy.save()

        hb = Entity(name='Harrison Barnes',
                    slug='harrison-barnes',
                    description="once played for the Warriors",
                    media_url='http://www.images.com/HarrisonBarnes.jpg',
                    owner=self.user)

        hb.save()

        EntityLike.objects.get_or_create(
            user=self.test_user,
            entity=steph
        )

        EntityLike.objects.get_or_create(
            user=self.test_user2,
            entity=kd
        )

        EntityLike.objects.get_or_create(
            user=self.test_user,
            entity=iggy
        )

        EntityLike.objects.get_or_create(
            user=self.test_user2,
            entity=hb
        )

    def test_list(self):
        response = self.client.get('/api/entities/')

        self.assertContains(response, 'Stephen Curry')
        self.assertContains(response, 'small forward')
        self.assertNotContains(response, 'Klay Thompson')
        self.assertContains(response, 'evanzamir')

    def test_lookup_slug(self):
        response = self.client.get('/api/entities/stephen-curry/')

        self.assertContains(response, 'Stephen Curry')
        self.assertNotContains(response, 'Kevin Durant')
        self.assertCountEqual(response.data['tags'], ['point_guard', 'mvp', '3pt', 'warriors'])

    def test_create(self):
        client = APIClient()
        client.force_authenticate(self.test_user)
        response = client.post(
            path='/api/entities/',
            data={
                'name': 'Draymond Green',
                'slug': 'draymond-green',
                'description': '2017 DPOY power forward for the Warriors',
                'media_url': 'http://www.nba-images.com/Draymond-Green.jpg',
                'tags': ['dpoy', 'power forward', 'small ball', 'warriors']
            })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        player = Entity.objects.get(tags__name__in=['dpoy'])
        self.assertEqual(player.slug, 'draymond-green')
        client.logout()

    def test_update(self):
        client = APIClient()
        client.force_authenticate(self.test_user)

        response = client.post(
            path='/api/entities/',
            data={
                'name': 'Klay Thompson',
                'slug': 'klay-thompson',
                'description': 'great 3pt shooter',
                'media_url': 'http://www.nba-images.com/KlayThompson.jpg',
                'tags': ['rocco']
            })

        updated = response.data['updated']

        response = client.patch(
            path='/api/entities/klay-thompson/',
            data={
                'description': '2nd best 3pt shooter in history!',
                'media_url': 'http://www.images.com/KlayThompsonUpdatedPic.jpg',
                'tags': ['3pt contest', 'warriors', 'shooting guard', 'rocco']
            }
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['description'], '2nd best 3pt shooter in history!')
        self.assertEqual(response.data['media_url'], 'http://www.images.com/KlayThompsonUpdatedPic.jpg')
        self.assertEqual(len(response.data['tags']), 4)
        self.assertGreater(response.data['updated'], updated)
        client.logout()

    def test_delete(self):
        client = APIClient()
        client.force_authenticate(self.user)

        response = client.delete(
            path='/api/entities/stephen-curry/'
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        response = client.get(
            path='/api/entities/'
        )

        self.assertEqual(len(response.data), 3)
        client.logout()

    def test_search_tags(self):
        client = APIClient()
        client.force_authenticate(self.test_user)
        response = client.post(
            path='/api/entities/tags/',
            data={
                'tags': ['point_guard']
            }
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_search_name(self):
        client = APIClient()
        client.force_authenticate(self.test_user)
        response = client.post(
            path='/api/entities/names/',
            data={
                'name': 'Kev'
            }
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_search_user(self):
        client = APIClient()
        client.force_authenticate(self.test_user)
        response = client.post(
            path='/api/entities/user/',
            data={
                'user': 'evanzamir'
            }
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)

        response = client.post(
            path='/api/entities/user/',
            data={
                'user': 'thecity2'
            }
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_entity_like_create(self):
        client = APIClient()
        client.force_authenticate(self.test_user)
        sdot = Entity(name='Shaun Livingston',
                      slug='shaun-livingston',
                      description='Point guard for the Warriors',
                      media_url='http://www.images.com/ShaunLivingston.jpg',
                      owner=self.user)

        sdot.save()
        sdot.tags.add('bench', 'knee injury', 'warriors', 'turnaround')

        response = client.post(
            path='/api/entity-likes/',
            data={
                'entity': sdot.id
            }
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_get_likes_by_user(self):
        client = APIClient()
        client.force_authenticate(self.test_user)

        response = client.get(
            path='/api/entity-likes/'
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0]['user'], 'thecity2')
        client.logout()
