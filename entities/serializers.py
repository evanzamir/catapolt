from django.utils.text import slugify
from django.core.exceptions import ObjectDoesNotExist

from rest_framework import serializers
from taggit_serializer.serializers import TagListSerializerField

from entities.models import Entity, EntityLike


class EntitySerializer(serializers.ModelSerializer):
    """
    Serialize and de-serialize entity objects
    """
    owner = serializers.CharField(source='owner.username', read_only=True)
    tags = TagListSerializerField()

    class Meta:
        model = Entity
        fields = ('id', 'created', 'name', 'description', 'media_url', 'moderation_code',
                  'owner', 'updated', 'tags', 'slug', 'uuid')

    def create(self, validated_data):
        try:
            entity = Entity.objects.get(slug=slugify(validated_data['name']))
        except ObjectDoesNotExist:
            entity = Entity.objects.create(owner=validated_data.pop('owner'),
                                           **validated_data)
            e = Entity.objects.get(slug=entity.slug)
            [e.tags.add(tag) for tag in validated_data['tags']]
        return entity


class EntityLikeSerializer(serializers.ModelSerializer):
    user = serializers.CharField(source='user.username', read_only=True)
    entity = EntitySerializer

    class Meta:
        model = EntityLike
        fields = ('id', 'created', 'updated', 'uuid', 'user', 'entity', 'moderation_code')


