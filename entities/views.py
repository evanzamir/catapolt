from django.contrib.auth.models import User
from rest_framework import permissions, viewsets
from rest_framework.authentication import SessionAuthentication, TokenAuthentication, BasicAuthentication
from rest_framework.decorators import list_route, detail_route
from rest_framework.response import Response

from entities.models import Entity, EntityLike
from scouting.permissions import IsOwnerOrReadOnly
from entities.serializers import EntitySerializer, EntityLikeSerializer


class EntityViewSet(viewsets.ModelViewSet):
    """
    This ViewSet performs create, retrieve, update, delete and also some custom methods:
    search by name, user, and tags

    """
    queryset = Entity.objects.all()
    serializer_class = EntitySerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsOwnerOrReadOnly,)
    lookup_field = 'slug'

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    @list_route(methods=['post'])
    def tags(self, request):
        """
        search for entities by tags they contain
        :param request:
        :return: Response
        """
        entities = Entity.objects.filter(tags__name__in=request.data['tags'])
        page = self.paginate_queryset(entities)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(entities, many=True)
        return Response(
            data=serializer.data
        )

    @list_route(methods=['post'])
    def names(self, request):
        """
        search for entities by name entity starts with (use for partial matching)
        :param request:
        :return: Response
        """
        entities = Entity.objects.filter(name__startswith=request.data['name'])
        page = self.paginate_queryset(entities)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(entities, many=True)
        return Response(
            data=serializer.data
        )

    @list_route(methods=['post'])
    def user(self, request):
        """
        search for entities by username of owner
        :param request:
        :return: Response
        """
        owner = User.objects.get(username=request.data['user'])
        entities = Entity.objects.filter(owner=owner)
        page = self.paginate_queryset(entities)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(entities, many=True)
        return Response(
            data=serializer.data
        )


class EntityLikeViewSet(viewsets.ModelViewSet):
    """
    This ViewSet performs create, retrieve, update, delete and also some custom methods:
    search by name, user, and tags

    """
    queryset = EntityLike.objects.all()
    serializer_class = EntityLikeSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsOwnerOrReadOnly,)
    lookup_field = 'user'

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_queryset(self):
        user = self.request.user
        return user.entity_likes.all()


