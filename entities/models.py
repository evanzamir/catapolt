import uuid as uuid_lib

from django.db import models
from taggit.managers import TaggableManager

from scouting.core import TimestampModerated


# Create your models here.
class Entity(TimestampModerated):
    """
    Entities are things like players or teams. These are the things being ranked by users.
    """
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=255)
    slug = models.SlugField(unique=True, blank=True)
    description = models.TextField(verbose_name='description of entity', blank=True)
    media_url = models.URLField(verbose_name='media representing entity (image, mp3, mpg, etc)',
                                blank=True, null=True)
    uuid = models.UUIDField(db_index=True, default=uuid_lib.uuid4, editable=False)
    owner = models.ForeignKey('auth.User', related_name='entities', on_delete=models.CASCADE)
    tags = TaggableManager(blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "entities"


class EntityLike(TimestampModerated):
    """
    Track which users like which players
    """

    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey('auth.User', related_name='entity_likes', on_delete=models.CASCADE)
    entity = models.ForeignKey(Entity)
    uuid = models.UUIDField(default=uuid_lib.uuid4, editable=False)

    class Meta:
        verbose_name_plural = "entity-likes"
        unique_together = ('user', 'entity')
