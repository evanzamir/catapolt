from django.db import models


# Create your models here.
class TimestampModerated(models.Model):
    """
    Inherit from this model to get created/updated datetime fields and moderation_code field
    """
    created = models.DateTimeField(auto_now_add=True, verbose_name='datetime object first created')
    moderation_code = models.PositiveSmallIntegerField(default=0, verbose_name='moderation status code')
    updated = models.DateTimeField(auto_now=True, verbose_name='datetime object updated')

    class Meta:
        abstract = True