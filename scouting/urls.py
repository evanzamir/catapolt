"""scouting URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.shortcuts import render

# def index(request):
#     return render(request, 'index.html')


admin.autodiscover()

urlpatterns = [
    url(r'^api/', include('polls.urls', namespace='polls')),
    url(r'^api/', include('profiles.urls', namespace='profiles')),
    url(r'^api/', include('photos.urls', namespace='urls')),
    url(r'^api/', include('votes.urls', namespace='votes')),
    url(r'^api/', include('comments.urls', namespace='comments')),
    url(r'^admin/', admin.site.urls),
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),
]
