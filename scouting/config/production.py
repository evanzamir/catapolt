from .base import *

SECRET_KEY = env('DJANGO_SECRET_KEY', default=None)

ALLOWED_HOSTS = ['.catapolt.co']

DATABASES = {
    'default': env.db()
}
