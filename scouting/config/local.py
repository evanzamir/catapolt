from .base import *

SECRET_KEY = env('DJANGO_SECRET_KEY', default='y0!4qmccb93*@$fp&^5(59%ax@#%t)y@uh)4q15+03*(lly-kg')

DEBUG = env.bool('DJANGO_DEBUG', default=True)
CORS_ORIGIN_ALLOW_ALL=True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'duopolly',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

EMAIL_HOST = 'localhost'
EMAIL_PORT = 25