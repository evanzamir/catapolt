from rest_framework import viewsets, status
from rest_framework.authentication import TokenAuthentication, BasicAuthentication


from comments.models import PhotoComment
from comments.permissions import IsOwnerOrReadOnly
from comments.serializers import PhotoCommentSerializer


class PhotoCommentViewSet(viewsets.ModelViewSet):
    """
    This viewsetomatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    """
    queryset = PhotoComment.objects.all()
    serializer_class = PhotoCommentSerializer
    authentication_classes = (TokenAuthentication, )
    permission_classes = (IsOwnerOrReadOnly,)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)


