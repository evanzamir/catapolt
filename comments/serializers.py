from rest_framework import serializers

from comments.models import PhotoComment
from profiles.serializers import ProfileSerializer


class PhotoCommentSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer(read_only=True)

    class Meta:
        model = PhotoComment
        fields = ('id', 'created', 'moderation_code', 'updated', 'owner', 'text',
                  'photo')

