from django.contrib import admin

# Register your models here.
from comments.models import PhotoComment
from polls.models import PollComment


# Register your models here.

class PhotoCommentAdmin(admin.ModelAdmin):
    model = PhotoComment
    list_display = ['text', 'photo', 'owner', 'created', 'updated']


admin.site.register(PhotoComment, PhotoCommentAdmin)
