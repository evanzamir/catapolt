from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from comments import views

router = DefaultRouter()
router.register(r'photo_comments', views.PhotoCommentViewSet)

app_name = 'comments'
urlpatterns = [
    url(r'^', include(router.urls))
]
