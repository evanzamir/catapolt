from uuid import uuid4

from django.db import models

from scouting.core import TimestampModerated
from scouting.settings import AUTH_USER_MODEL
from profiles.models import Profile
from photos.models import Photo


class AbstractComment(TimestampModerated):
    id = models.UUIDField(db_index=True, default=uuid4, editable=False, primary_key=True)
    owner = models.ForeignKey(AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    text = models.CharField(max_length=255)

    class Meta:
        abstract = True

    def __str__(self):
        return self.text


class PhotoComment(AbstractComment):
    photo = models.ForeignKey(Photo, related_name='photo_comments', on_delete=models.CASCADE, null=True)

    class Meta:
        verbose_name_plural = "comments"



