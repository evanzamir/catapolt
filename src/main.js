import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import {routes} from './routes'
import axios from 'axios'
import store from './store'
import VueYouTubeEmbed from 'vue-youtube-embed'
import SocialSharing from 'vue-social-sharing'

axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = 'X-CSRFToken'
// axios.defaults.baseURL = 'http://localhost:8000'
axios.defaults.baseURL = 'https://catapolt.co'
axios.defaults.timeout = 10000

Vue.use(Vuetify, {})
Vue.use(VueRouter)
Vue.use(require('vue-moment'))
Vue.use(VueYouTubeEmbed)
Vue.use(SocialSharing)

const router = new VueRouter({
    routes,
    mode: 'history'
});

new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App),
     created () {
        this.$store.commit('START_LOADING')
        console.log(this.$route)
        this.$store.dispatch('authenticate').then(res => {
            console.log(res)
            return this.$store.dispatch('getUserInfo')
        }).then(res => {
            this.$store.commit('SET_USERID', res.data.pk)
            this.$store.commit('SET_USERNAME', res.data.username)
            return this.$store.dispatch('profile')
        }).then(res => {
            this.$store.commit('SET_USER_PROFILE', res.data)
            this.$store.commit('STOP_LOADING')
            this.$router.push({name: this.$route.name || 'polls', query: this.$route.query,
                params: this.$route.params})
        }).catch(err => {
            console.log(err)
            if (this.$route.name==='polls') {
                this.$router.push({name: 'home'})
            } else {
                this.$router.push({name: this.$route.name})
            }
            this.$store.commit('STOP_LOADING')
        })
    }
})
