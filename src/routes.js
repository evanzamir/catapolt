import ProfileEdit from './components/ProfileEdit.vue'
import Home from './components/Home.vue'
import Login from './components/Login.vue'
import Signup from './components/Signup.vue'
import Polls from './components/Polls.vue'
import PollDetail from './components/PollDetail.vue'
import UserPolls from './components/UserPolls.vue'

export const routes = [
    {path: '/welcome', name: 'home', component: Home},
    {path: '/login', name: 'login', component: Login},
    {path: '/signup', name: 'signup', component: Signup},
    {path: '/profile/:userid', name: 'profile', component: ProfileEdit},
    {path: '/polls/:id/:slug', name: 'poll', component: PollDetail},
    {path: '/user/:username/:userid', name: 'user', component: UserPolls},
    // {path: '/polls', name: 'polls', component: Polls},
    {path: '/', name: 'polls', component: Polls}
]