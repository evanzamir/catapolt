import hashMod from 'hash-mod'

const
    vuetifyColors = [
        'red',
        'pink',
        'purple',
        'deep-purple',
        'indigo',
        'blue',
        'light-blue',
        'cyan',
        'teal',
        'green',
        'light-green',
        'lime',
        'yellow',
        'amber',
        'orange',
        'deep-orange',
        'brown',
        'blue-grey',
        'grey'
    ]

function hashName(name) {
    let mod = hashMod(19)
    return vuetifyColors[mod(name)]
}

export { hashName }