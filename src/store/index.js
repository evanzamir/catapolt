/**
 * Created by evanzamir on 7/29/17.
 */
import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        userProfile: null,
        token: null,
        userid: null,
        username: null,
        communityTags: [],
        loading: false,
        light: false,
        portfolioUserid: null,
        gridOn: true,
        polls: [],
        userPolls: [],
        activePolls: [],
        closedPolls: []
    },
    mutations: {
        SET_TOKEN_AUTH(state, key) {
            state.token = key
        },
        SET_USERID(state, id) {
            state.userid = id
        },
        SET_USERNAME(state, username) {
            state.username = username
        },
        USER_LOGOUT(state) {
            state.userProfile = null
            state.token = null
            state.userid = null
            state.username = null
            state.portfolioPhotos = []
            state.portfolioSearchTags = []
            state.portfolioTags = []
            state.communitySearchTags = []
            state.communityPhotos = []
            state.communityTags = []
            state.submittedPhotos = []
            state.light = false
            state.portfolioUserid = null
            state.gridOn = true
            state.polls = []
            state.userPolls = []
            state.activePolls = []
            state.closedPolls = []
        },
        SET_PORTFOLIO_PHOTOS(state, photos) {
            state.portfolioPhotos = photos
        },
        SET_PORTFOLIO_TAGS(state, tags) {
            state.portfolioTags = tags
        },
        SET_PORTFOLIO_SEARCH_TAGS(state, tags) {
            state.portfolioSearchTags = tags
        },
        ADD_PORTFOLIO_PHOTO(state, photo) {
            state.portfolioPhotos.unshift(photo)
        },
        ADD_SUBMITTED_PHOTO(state, photo) {
            state.submittedPhotos.unshift(photo)
        },
        DELETE_PORTFOLIO_PHOTO(state, index) {
            state.portfolioPhotos.splice(index, 1)
        },
        DELETE_COMMUNITY_PHOTO(state, index) {
            state.communityPhotos.splice(index, 1)
        },
        DELETE_SUBMITTED_PHOTO(state, index) {
            state.submittedPhotos.splice(index, 1)
        },
        SET_COMMUNITY_PHOTOS(state, photos) {
            state.communityPhotos = photos
        },
        SET_COMMUNITY_TAGS(state, tags) {
            state.communityTags = tags
        },
        START_LOADING(state) {
            state.loading = true
        },
        STOP_LOADING(state) {
            state.loading = false
        },
        DECREMENT_COMMUNITY_PHOTO_NUM_COMMENTS(state, payload) {
            state.communityPhotos[payload.index].num_comments -= 1
        },
        TOGGLE_LIGHT(state) {
            state.light = !state.light
        },
        TOGGLE_GRID(state) {
            state.gridOn = !state.gridOn
        },
        SET_PORTFOLIO_USERID(state, id) {
            state.portfolioUserid = id
        },
        SET_COMMUNITY_PHOTO_LIKES(state, payload) {
            state.communityPhotos[payload.index].likes = payload.photo.likes
            state.communityPhotos[payload.index].num_likes = payload.photo.num_likes
        },
        SET_PORTFOLIO_PHOTO_LIKES(state, payload) {
            state.portfolioPhotos[payload.index].likes = payload.photo.likes
            state.portfolioPhotos[payload.index].num_likes = payload.photo.num_likes
        },
        SET_USER_PROFILE(state, profile) {
            state.userProfile = profile
        },
        SET_ACTIVE_POLLS(state, polls) {
            state.activePolls = polls
        },
        SET_CLOSED_POLLS(state, polls) {
            state.closedPolls = polls
        },
        SET_USER_POLLS(state, polls) {
            state.userPolls = polls
        },
        SET_POLL_FOLLOWS(state, payload) {
            if (payload.state==='active') {
                state.activePolls[payload.index].follows = payload.poll.follows
            } else if (payload.state==='closed') {
                state.closedPolls[payload.index].follows = payload.poll.follows
            }
        },
    },
    getters: {
        getGridOn(state) {
            return state.gridOn
        },
        getLighting(state) {
            return state.light
        },
        getUserCredits(state) {
            return state.user_credits
        },
        getVotesUnlock(state) {
            return state.votes_unlock
        },
        getAuthToken(state) {
            return state.token
        },
        isAuthenticated(state) {
            if (state.token) {
                return true
            } else {
                return false
            }
        },
        getUserid(state) {
            return state.userid
        },
        getUsername(state) {
            return state.username
        },
        getPortfolioPhotos(state) {
            return state.portfolioPhotos
        },
        getPortfolioTags(state) {
            return state.portfolioTags
        },
        getPortfolioSearchTags(state) {
            return state.portfolioSearchTags
        },
        getCommunityPhotos(state) {
            return state.communityPhotos
        },
        getCommunityTags(state) {
            return state.communityTags
        },
        getLoading(state) {
            return state.loading
        },
        getPortfolioUserid(state) {
            return state.portfolioUserid
        },
        getUserProfile(state) {
            return state.userProfile
        },
        getUserPolls(state) {
            return state.userPolls
        },
        getActivePolls(state) {
            return state.activePolls
        },
        getClosedPolls(state) {
            return state.closedPolls
        }
    },
    actions: {
        authenticate({commit, state}, payload) {
            const token = localStorage.getItem("token")
            if (token) {
                commit('SET_TOKEN_AUTH', token)
                return Promise.resolve(token)
            }
            else {
                return Promise.reject(new Error('no token'))
            }
        },
        storeToken({commit, state}, token) {
            localStorage.setItem('token', state.token)
        },
        deleteToken({commit, state}) {
            localStorage.removeItem('token')
        },
        logout({commit, state}) {
            return axios.post('/rest-auth/logout/', {}, {
                headers: {
                    'Authorization': 'Token ' + state.token
                }
            })
        },
        signup({commit}, payload) {
            return axios.post('/rest-auth/registration/', {
                username: payload.username,
                password1: payload.password1,
                password2: payload.password2,
                email: payload.email
            })
        },
        login({commit}, payload) {
            return axios.post('/rest-auth/login/', {
                username: payload.username,
                password: payload.password
            })
        },
        profile({state}) {
            return axios.get(`/api/profiles/${state.userid}`, {
                headers: {
                    'Authorization': 'Token ' + state.token,
                    'Content-Type': 'application/json'
                }
            })
        },
        userProfile({state}, payload) {
            return axios.get(`/api/profiles/${payload.id}`, {
                headers: {
                    // 'Authorization': 'Token ' + state.token,
                    'Content-Type': 'application/json'
                }
            })
        },
        updateProfileInfo({state}, payload) {
            const formData = new FormData()
            formData.append('bio', payload.bio)
            formData.append('media_url', payload.media_url)
            formData.append('first_name', payload.first_name)
            formData.append('last_name', payload.last_name)
            return axios.patch(`/api/profiles/${state.userid}/`, formData, {
                headers: {
                    'Authorization': 'Token ' + state.token,
                    'Content-Type': 'multipart/form-data'
                }
            })
        },
        searchCommunityTags({commit, state}, payload) {
            return axios.get('/api/photos/community/',
                {
                    params: {
                        tags: payload.tags,
                        sortType: payload.sortType
                    },
                    headers: {
                        'Authorization': 'Token ' + state.token,
                        'Content-Type': 'application/json'
                    }
                })
        },
        getCommunityTags({commit, state}) {
            return axios.get('/api/photos/tag_list/', {
                headers: {
                    'Content-Type': 'multipart/form-data',
                }
            })
        },
        getUserInfo({state}) {
            return axios.get('/rest-auth/user/', {
                headers: {
                    'Authorization': 'Token ' + state.token
                }
            })
        },
        vote({state}, payload) {
            const formData = new FormData()
            formData.append('winner', payload.winner)
            formData.append('loser', payload.loser)
            return axios.post('/api/votes/', formData, {
                headers: {
                    'Authorization': 'Token ' + state.token,
                    'Content-Type': 'multipart/form-data'
                }
            })
        },
        addPollComment({state}, payload) {
            return axios.post('/api/comments/', {
                text: payload.text,
                poll: payload.id
            }, {
                headers: {
                    Authorization: 'Token ' + state.token
                }
            })
        },
        getPollComments({state}, id) {
            return axios.get(`/api/polls/${id}/comments/`, {}, {
                headers: {
                    'Authorization': 'Token ' + state.token,
                    'Content-Type': 'application/json'
                }
            })
        },
        deletePollComment({state}, id) {
            return axios.delete(`/api/comments/${id}/`, {
                headers: {
                    'Authorization': 'Token ' + state.token,
                }
            })
        },
        reportPhoto({state}, payload) {
            const formData = new FormData()
            formData.append('status', parseInt(payload.status))
            return axios.post(`/api/photos/${payload.id}/report/`, formData, {
                headers: {
                    'Authorization': 'Token ' + state.token,
                    'Content-Type': 'application/json'
                }
            })
        },
        createPoll({state}, payload) {
            const formData = new FormData()
            formData.append('title', payload.title)
            formData.append('description', payload.description)
            formData.append('tags', JSON.stringify(payload.tags))
            formData.append('photo', payload.photo)
            formData.append('private', payload.isPollPrivate)
            return axios.post(`/api/polls/`,
                formData,
                {
                    headers: {
                        'Authorization': 'Token ' + state.token,
                        'Content-Type': 'multipart/form-data'
                    }
                })
            },
        deletePoll({state}, id) {
            return axios.delete(`/api/polls/${id}`,
                {
                    headers: {
                        'Authorization': 'Token ' + state.token,
                        'Content-Type': 'multipart/form-data'
                    }
                })
        },
        deleteEntry({state}, id) {
            return axios.delete(`/api/entries/${id}`,
                {
                    headers: {
                        'Authorization': 'Token ' + state.token,
                        'Content-Type': 'multipart/form-data'
                    }
                })
        },
        startPoll({state}, id) {
            return axios.post(`/api/polls/${id}/activate/`, {}, {
                headers: {
                    'Authorization': 'Token ' + state.token,
                    'Content-Type': 'multipart/form-data'
                }
            })
        },
        closePoll({state}, id) {
            return axios.post(`/api/polls/${id}/close/`, {}, {
                headers: {
                    'Authorization': 'Token ' + state.token,
                    'Content-Type': 'multipart/form-data'
                }
            })
        },
        getPollSample({state}, id) {
            return axios.get(`/api/polls/${id}/sample/`, {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
        },
        getPollChoice({state}, id) {
            return axios.get(`/api/polls/${id}/choice/`, {
                headers: {
                    'Authorization': 'Token ' + state.token,
                    'Content-Type': 'application/json'
                }
            })
        },
        submitPollEntry({state}, payload) {
            const formData = new FormData()
            formData.append('title', payload.title)
            formData.append('hash', payload.hash)
            formData.append('description', payload.description)
            formData.append('size', payload.size)
            formData.append('photo', payload.photo)
            formData.append('poll', payload.poll)
            formData.append('tags', JSON.stringify(payload.tags))
            return axios.post(`/api/entries/`, formData, {
                headers: {
                    'Authorization': 'Token ' + state.token,
                    'Content-Type': 'multipart/form-data'
                }
            })
        },
        getPoll({state}, id) {
            return axios.get(`/api/polls/${id}/`, {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
        },
        getPollEntries({state}, id) {
            return axios.get(`/api/polls/${id}/entries/`, {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
        },
        searchActivePolls({state}, query) {
            return axios.get(`/api/polls/active/`,
                {
                    params: {
                        searchQuery: query
                    },
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
        },
        searchClosedPolls({state}, query) {
            return axios.get(`/api/polls/closed/`,
                {
                    params: {
                        searchQuery: query
                    },
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
        },
        getActivePolls({state}) {
            return axios.get(`/api/polls/all_active/`, {
                headers: {
                    // 'Authorization': 'Token ' + state.token,
                    'Content-Type': 'application/json',
                }
            })
        },
        getClosedPolls({state}) {
            return axios.get(`/api/polls/all_closed/`, {
                headers: {
                    // 'Authorization': 'Token ' + state.token,
                    'Content-Type': 'application/json',
                }
            })
        },
        getUserPolls({state}, id) {
            return axios.get(`/api/profiles/${id}/polls/`,
                {
                    headers: {
                        'Content-Type': 'application/json',
                   }
                })
        },
        getUserOpenPolls({state}, id) {
            return axios.get(`/api/profiles/${id}/open/`,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
        },
        getUserOpenPollsPublic({state}, id) {
            return axios.get(`/api/profiles/${id}/public_open/`,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
        },
        getUserClosedPolls({state}, id) {
            return axios.get(`/api/profiles/${id}/closed/`,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
        },
        getUserClosedPollsPublic({state}, id) {
            return axios.get(`/api/profiles/${id}/public_closed/`,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
        },
        getUserUnactivatedPolls({state}, id) {
            return axios.get(`/api/profiles/${id}/unactivated/`,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
        },
        getUserSavedPolls({state}, id) {
            return axios.get(`/api/profiles/${id}/saved/`,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
        },
        getUserSavedPollsPublic({state}, id) {
            return axios.get(`/api/profiles/${id}/saved_public/`,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
        },
        followPollToggled({state}, payload) {
            return axios.post(`/api/polls/${payload.id}/follow/`, {}, {
                headers: {
                    'Authorization': 'Token ' + state.token,
                    'Content-Type': 'application/json'
                }
            })
        },
        pollEntryVote({state}, payload) {
            const formData = new FormData()
            formData.append('combination', payload.combo)
            formData.append('poll', payload.poll)
            formData.append('winner', payload.winner)
            formData.append('loser', payload.loser)
            if (payload.draw) {
                formData.append('draw', true)
            }
            return axios.post(`/api/votes/`, formData, {
                headers: {
                    'Authorization': 'Token ' + state.token,
                    'Content-Type': 'application/json'
                }
            })
        },
    }
})

