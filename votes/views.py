from rest_framework import viewsets
from rest_framework.parsers import MultiPartParser, FormParser

from votes.models import Vote
from profiles.models import Profile
from votes.permissions import IsOwnerOrReadOnly
from votes.serializers import VoteSerializer


class VoteViewSet(viewsets.ModelViewSet):
    """
    This viewsetomatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """
    queryset = Vote.objects.all()
    serializer_class = VoteSerializer
    permission_classes = (IsOwnerOrReadOnly,)
    parser_classes = (MultiPartParser, FormParser)

    def perform_create(self, serializer):
        profile = Profile.objects.get(id=self.request.user.id)
        serializer.save(profile=profile)
