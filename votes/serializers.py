from uuid import UUID

from django.core.exceptions import ObjectDoesNotExist

from rest_framework import serializers

from votes.models import Vote
from profiles.models import Profile
from profiles.serializers import ProfileSerializer


class VoteSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer(read_only=True)

    class Meta:
        model = Vote
        fields = (
            'id', 'created', 'moderation_code', 'profile', 'updated', 'winner',
            'loser')

    def create(self, validated_data):
        try:
            winner = validated_data['winner']
            loser = validated_data['loser']

            winner.wins.add(loser)
            loser.losses.add(winner)

            winner.win_pct = winner.wins.count()/(winner.wins.count()+winner.losses.count())
            winner.save()
            loser.win_pct=loser.wins.count()/(loser.losses.count()+loser.wins.count())
            loser.save()

            vote = Vote.objects.create(
                **validated_data
            )
            profile = validated_data['profile']

            profile.votes_to_unlock_credit = profile.votes_to_unlock_credit - 1
            if profile.votes_to_unlock_credit <= 0:
                profile.credits += 1
                profile.votes_to_unlock_credit += 10
            profile.save()
            return vote
        except ObjectDoesNotExist:
            return ObjectDoesNotExist
