from django.contrib import admin

from votes.models import Vote


# Register your models here.

class VoteAdmin(admin.ModelAdmin):
    model = Vote
    list_display = ['id', 'winner', 'loser', 'created', 'updated', 'profile']


admin.site.register(Vote, VoteAdmin)
