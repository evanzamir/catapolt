from uuid import uuid4

from django.db import models
from photos.models import Photo
from scouting.core import TimestampModerated
from scouting.settings import AUTH_USER_MODEL


class Vote(TimestampModerated):
    id = models.UUIDField(db_index=True, default=uuid4, editable=False, primary_key=True)
    profile = models.ForeignKey(AUTH_USER_MODEL, related_name='votes')
    winner = models.ForeignKey(Photo, related_name='winner', blank=True, null=True)
    loser = models.ForeignKey(Photo, related_name='loser', blank=True, null=True)

    class Meta:
        verbose_name_plural = "votes"
