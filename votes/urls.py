from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from votes import views

router = DefaultRouter()
router.register(r'votes', views.VoteViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
]
