from uuid import uuid4

from django.db import models
from django.core.exceptions import ValidationError

from scouting.core import TimestampModerated
from scouting.settings import AUTH_USER_MODEL


# Create your models here.
class Follow(TimestampModerated):
    id = models.UUIDField(db_index=True, default=uuid4, editable=False, primary_key=True)
    user = models.ForeignKey(AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='followed', blank=False)
    followed = models.ForeignKey(AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='followers', blank=False)

    class Meta:
        verbose_name_plural = "follows"

    def clean(self):
        if self.user == self.followed:
            raise ValidationError('User cannot follow self.')

        follows = Follow.objects.filter(user=self.user, followed=self.followed)
        if len(follows) > 0:
            raise ValidationError('user already has follow relation.')

