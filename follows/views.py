from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

from rest_framework import permissions, viewsets
from rest_framework.exceptions import ValidationError
from rest_framework.authentication import SessionAuthentication
from rest_framework.response import Response
from rest_framework.decorators import list_route, detail_route
from rest_framework import status

from follows.models import Follow
from follows.permissions import IsOwnerOrReadOnly
from follows.serializers import FollowSerializer


class FollowViewSet(viewsets.ModelViewSet):
    """
    list:
    Return of list of follow relations for a user.

    create:
    Creates a follow relation for a user.

    read:
    Check whether user follows a specific user.

    delete:
    Delete a follow relation.
    """
    queryset = Follow.objects.all()
    serializer_class = FollowSerializer
    authentication_classes = (SessionAuthentication,)
    permission_classes = (permissions.AllowAny, IsOwnerOrReadOnly)
    lookup_field = 'followed'

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_queryset(self):
        user = self.request.user
        return user.followed.all()

    @list_route()
    def followers(self, request):
        """
        List of followers for user.
        """
        followers = Follow.objects.filter(followed=self.request.user)
        page = self.paginate_queryset(followers)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(followers, many=True)
        return Response(
            data=serializer.data
        )
