from django.core.exceptions import ObjectDoesNotExist

from rest_framework import serializers
from rest_framework import status

from follows.models import Follow


class FollowSerializer(serializers.ModelSerializer):
    user = serializers.CharField(source='user.username', read_only=True)

    class Meta:
        model = Follow
        fields = ('id', 'uuid', 'user', 'followed', 'created', 'updated', 'moderation_code')

    def create(self, validated_data):
        try:
            Follow.objects.get(**validated_data)
            raise serializers.ValidationError("User relation already exists.")
        except ObjectDoesNotExist:
            follow = Follow.objects.create(**validated_data)
        return follow

