from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from follows import views

router = DefaultRouter()
router.register(r'follows', views.FollowViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
]
