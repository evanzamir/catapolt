from django.contrib.auth import models
from rest_framework import test, status
from rest_framework.test import APIClient

from follows.models import Follow


class FollowsAPITests(test.APITestCase):
    """
    Testing django-rest-framework API
    """

    def setUp(self):
        self.admin, result = models.User.objects.get_or_create(username='evanzamir', password='mazda603')
        self.user1, result = models.User.objects.get_or_create(username='judah', password='judah2016')
        self.user2, result = models.User.objects.get_or_create(username='ellie', password='eliana2014')
        self.user3, result = models.User.objects.get_or_create(username='charlie', password='charlie2014')
        self.user4, result = models.User.objects.get_or_create(username='julian', password='julian2017')
        self.user5, result = models.User.objects.get_or_create(username='georgina', password='georgina2011')

    def test_create_follow(self):
        new_user, result = models.User.objects.get_or_create(username='mark', password='dinosaur')
        client = APIClient()
        client.force_authenticate(user=new_user)

        response = client.post(
            path='/api/follows/',
            data={
                'followed': self.user1.id
            }
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['followed'], self.user1.id)
        client.logout()

    def test_unique_user_followed_relations(self):
        new_user, result = models.User.objects.get_or_create(username='mark', password='dinosaur')
        Follow.objects.get_or_create(user=new_user, followed=self.user1)

        client = APIClient()
        client.force_authenticate(user=new_user)
        response = client.post(
            path='/api/follows/',
            data={
                'followed': self.user1.id
            }
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data.pop(), "User relation already exists.")
        client.logout()

    def test_get_followed_for_user(self):
        Follow.objects.get_or_create(user=self.user1, followed=self.user2)
        Follow.objects.get_or_create(user=self.user1, followed=self.user3)
        Follow.objects.get_or_create(user=self.user1, followed=self.user4)
        Follow.objects.get_or_create(user=self.user1, followed=self.user5)

        client = APIClient()
        client.force_authenticate(user=self.user1)
        response = client.get(
            path='/api/follows/'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 4)

    def test_get_followers_for_user(self):
        Follow.objects.get_or_create(user=self.user2, followed=self.user1)
        Follow.objects.get_or_create(user=self.user3, followed=self.user1)
        Follow.objects.get_or_create(user=self.user4, followed=self.user1)

        client = APIClient()
        client.force_authenticate(user=self.user1)
        response = client.get(
            path='/api/follows/followers/'
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)

    def test_validate_user_follows_other_user(self):
        Follow.objects.get_or_create(user=self.user1, followed=self.user2)
        Follow.objects.get_or_create(user=self.user1, followed=self.user3)
        Follow.objects.get_or_create(user=self.user1, followed=self.user4)

        client = APIClient()
        client.force_authenticate(user=self.user1)
        response = client.get(
            path='/api/follows/{}/'.format(self.user2.id)
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_validate_user_does_not_follow_other_user(self):
        Follow.objects.get_or_create(user=self.user1, followed=self.user2)
        Follow.objects.get_or_create(user=self.user1, followed=self.user3)
        Follow.objects.get_or_create(user=self.user1, followed=self.user4)

        client = APIClient()
        client.force_authenticate(user=self.user1)

        response = client.get(
            path='/api/follows/{}/'.format(self.user5.id),
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_unfollow_user(self):
        Follow.objects.get_or_create(user=self.user1, followed=self.user2)
        client = APIClient()
        client.force_authenticate(user=self.user1)

        # now delete follow using id
        response = client.delete(
            path='/api/follows/{}/'.format(self.user2.id)
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        # try to lookup follow relation again

        response = client.get(
            path='/api/follows/{}/'.format(self.user2.id)
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
