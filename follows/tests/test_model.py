from django.contrib.auth import models
from django.core.exceptions import ValidationError
from rest_framework import test

from follows.models import Follow


class FollowTest(test.APITestCase):
    def setUp(self):
        self.admin, result = models.User.objects.get_or_create(username='evanzamir', password='mazda603')
        self.user1, result = models.User.objects.get_or_create(username='judah', password='judah2016')
        self.user2, result = models.User.objects.get_or_create(username='ellie', password='eliana2014')
        self.user3, result = models.User.objects.get_or_create(username='charlie', password='charlie2014')
        self.user4, result = models.User.objects.get_or_create(username='julian', password='julian2017')
        self.user5, result = models.User.objects.get_or_create(username='georgina', password='georgina2011')

    def test_create_follows(self):
        Follow.objects.get_or_create(user=self.user1, followed=self.user2)
        Follow.objects.get_or_create(user=self.user1, followed=self.user3)
        Follow.objects.get_or_create(user=self.user1, followed=self.user4)
        Follow.objects.get_or_create(user=self.user1, followed=self.user5)
        Follow.objects.get_or_create(user=self.user2, followed=self.user1)
        Follow.objects.get_or_create(user=self.user2, followed=self.user3)
        Follow.objects.get_or_create(user=self.user2, followed=self.user4)
        Follow.objects.get_or_create(user=self.user2, followed=self.user5)

        follows = Follow.objects.all()
        self.assertEqual(len(follows), 8)

    def test_self_follow_raises_exception(self):
        with self.assertRaisesMessage(expected_exception=ValidationError,
                                      expected_message='User cannot follow self.'):
            follow = Follow(user=self.user1, followed=self.user1)
            follow.clean()

    def test_get_followers_for_user(self):
        Follow.objects.get_or_create(user=self.user2, followed=self.user1)
        Follow.objects.get_or_create(user=self.user3, followed=self.user1)
        Follow.objects.get_or_create(user=self.user4, followed=self.user1)
        Follow.objects.get_or_create(user=self.user5, followed=self.user1)

        user = models.User.objects.get(username='judah')
        followers = user.followers.all()
        self.assertEqual(len(followers), 4)

    def test_get_followed_for_user(self):
        Follow.objects.get_or_create(user=self.user1, followed=self.user2)
        Follow.objects.get_or_create(user=self.user1, followed=self.user3)
        Follow.objects.get_or_create(user=self.user1, followed=self.user4)
        Follow.objects.get_or_create(user=self.user1, followed=self.user5)

        user = models.User.objects.get(username='judah')
        print(user.get_full_name())
        follows = user.followed.all()
        self.assertEqual(len(follows), 4)

    def test_follow_should_be_unique(self):
        Follow.objects.get_or_create(user=self.user1, followed=self.user2)
        with self.assertRaisesMessage(expected_exception=ValidationError,
                                      expected_message='user already has follow relation.'):
            follow = Follow(user=self.user1, followed=self.user2)
            follow.clean()
