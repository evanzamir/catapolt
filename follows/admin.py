from django.contrib import admin

from follows.models import Follow


# Register your models here.

class FollowAdmin(admin.ModelAdmin):
    model = Follow
    list_display = ['id', 'user', 'followed', 'created', 'moderation_code', 'updated']


admin.site.register(Follow, FollowAdmin)
