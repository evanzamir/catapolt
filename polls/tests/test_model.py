from datetime import timedelta, datetime

from django.contrib.auth import models
from django.utils.text import slugify
from rest_framework import test

from entities.models import Entity
from polls.models import Poll, PollOption


class PollTest(test.APITestCase):
    def setUp(self):
        self.admin, result = models.User.objects.get_or_create(username='evanzamir', password='georgina2011')
        self.user, result = models.User.objects.get_or_create(username='thecity2', password='judah2016')

        self.poll = Poll(
            expiration=2,
            title='a test poll',
            description='testing if poll works',
            slug=slugify('a test poll'),
            owner=self.user
        )
        self.poll.save()
        self.poll.tags.add('test', 'nba', 'basketball', 'poll', 'warriors')

    def test_create_poll(self):
        poll = Poll(
            expiration=2,
            title='another test poll',
            slug=slugify('another test poll'),
            description='testing if poll works',
            owner=self.user
        )
        poll.save()
        poll.tags.add('test', 'nba', 'basketball', 'poll')
        self.assertEqual(poll.slug, 'another-test-poll')
        self.assertEqual(poll.title, 'another test poll')
        self.assertEqual(poll.description, 'testing if poll works')
        self.assertEqual(poll.expiration, 2)
        self.assertGreater(poll.expiration_date, poll.created + timedelta(days=1))
        self.assertLess(poll.expiration_date, poll.created + timedelta(days=3))
        self.assertEqual(poll.owner, self.user)

    def test_get_poll_by_tag(self):
        poll = Poll.objects.get(tags__name__in=['warriors'])
        self.assertEqual(poll.title, 'a test poll')
        self.assertCountEqual(poll.tags.names(), self.poll.tags.names())


class PollOptionTest(test.APITestCase):
    def setUp(self):
        self.admin, result = models.User.objects.get_or_create(username='evanzamir', password='georgina2011')
        self.user, result = models.User.objects.get_or_create(username='thecity2', password='judah2016')

        self.poll = Poll(
            expiration=2,
            title='a test poll',
            description='testing if poll works',
            owner=self.user,
            slug='a-test-poll',
            tags=['test', 'nba', 'basketball', 'poll']
        )
        self.poll.save()

        self.steph = Entity(name='Stephen Curry',
                            slug='stephen-curry',
                            description='Point guard for the Warriors',
                            media_url='http://www.images.com/StephenCurry.jpg',
                            owner=self.admin)

        self.steph.save()
        self.steph.tags.add('point_guard', 'mvp', '3pt', 'warriors')

    def test_create_poll_option(self):
        option = PollOption(
            poll=self.poll,
            entity=self.steph,
            description='a test description for the option',
            media_url='/img/test_image.jpg'
        )
        option.save()

        self.assertEqual(option.poll.uuid, self.poll.uuid)
        self.assertEqual(option.entity.uuid, self.steph.uuid)
        self.assertEqual(option.description, 'a test description for the option')
        self.assertEqual(option.media_url, '/img/test_image.jpg')
        self.assertEqual(option.poll.description, 'testing if poll works')
        self.assertEqual(option.poll.owner, self.user)
        self.assertEqual(option.entity.owner, self.admin)
