from django.contrib.auth import models
from django.utils.text import slugify
from rest_framework import test, status
from rest_framework.test import APIClient

from entities.models import Entity
from polls.models import Poll, PollOption


class PollAPITests(test.APITestCase):
    def setUp(self):
        models.User.objects.get_or_create(username='evanzamir', password='georgina2011')
        self.user = models.User.objects.get(username='evanzamir')
        self.test_user = models.User.objects.create(username='testuser', password='password')

        self.poll1, result = Poll.objects.get_or_create(
            expiration=2,
            title='most valuable player on Warriors',
            description='who is the Warriors MVP?',
            slug=slugify('most valuable player on Warriors'),
            owner=self.test_user
        )

        self.poll2, result = Poll.objects.get_or_create(
            expiration=7,
            title='Best shooter on Warriors',
            description='this poll is to find the best shooter on the Warriors',
            slug=slugify('Best shooter on Warriors'),
            owner=self.test_user
        )

        self.poll1.tags.add('mvp', 'warriors')
        self.poll1.save()

        self.poll2.save()

    def test_lookup_slug(self):
        response = self.client.get(
            path='/api/polls/{}/'.format(slugify('most valuable player on Warriors'))
        )
        self.assertEqual(response.data['title'], self.poll1.title)
        self.assertCountEqual(response.data['tags'], ['mvp', 'warriors'])

        response = self.client.get(
            path='/api/polls/{}/'.format(slugify('Best shooter on Warriors'))
        )
        self.assertEqual(response.data['uuid'], str(self.poll2.uuid))

    def test_get_poll_count(self):
        response = self.client.get(
            path='/api/polls/'
        )
        self.assertEqual(len(response.data), 2)

    def test_create_poll(self):
        client = APIClient()
        client.force_authenticate(self.test_user)

        response = client.post(
            '/api/polls/',
            data={
                'expiration': 2,
                'title': 'Most valuable Warrior in 2017 playoffs',
                'description': 'who was the best player in the warriors 2017 playoffs',
                'tags': ['nba', 'warriors', '2017 Finals', 'MVP']
            },
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        poll = Poll.objects.get(
            title='Most valuable Warrior in 2017 playoffs'
        )
        self.assertEqual(poll.title, 'Most valuable Warrior in 2017 playoffs')
        self.assertEqual(poll.slug, slugify('Most valuable Warrior in 2017 playoffs'))
        self.assertCountEqual(poll.tags.names(), response.data['tags'])
        client.logout()
