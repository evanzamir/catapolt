import uuid as uuid_lib
from datetime import timedelta

from django.utils.text import slugify
from django.db import models
from django.utils import timezone
from django.core.files.storage import default_storage
from taggit.managers import TaggableManager
from versatileimagefield.fields import VersatileImageField

from photos.models import UUIDTaggedItem, Photo
from comments.models import AbstractComment
from scouting.core import TimestampModerated
from scouting.settings import AUTH_USER_MODEL


def user_directory_path(instance, filename):
    return 'img/u/{}/{}'.format(instance.owner.id,
                                filename)


# Create your models here.
class Poll(TimestampModerated):
    CONTEST = 1
    POLL = 2
    CHOICES = (
        (CONTEST, "Contest"),
        (POLL, "Poll")
    )
    id = models.UUIDField(db_index=True, default=uuid_lib.uuid4, editable=False, primary_key=True)
    poll_type = models.PositiveSmallIntegerField(choices=CHOICES, default=POLL)
    title = models.CharField(max_length=80, unique=True, db_index=True)
    owner = models.ForeignKey(AUTH_USER_MODEL, related_name='polls', on_delete=models.CASCADE, db_index=True)
    start_date = models.DateTimeField(null=True, blank=True, editable=True)
    expiration_date = models.DateTimeField(null=True, blank=True, editable=True)
    description = models.TextField(max_length=255, verbose_name='description of poll', blank=False)
    slug = models.SlugField(blank=True)
    tags = TaggableManager(blank=True, through=UUIDTaggedItem)
    photo = VersatileImageField(upload_to=user_directory_path, height_field="height", width_field="width", blank=True)
    height = models.IntegerField(default=0)
    width = models.IntegerField(default=0)
    private = models.BooleanField(editable=True, default=False)
    users_threshold = models.IntegerField(default=0, editable=True)
    max_entries_per_user = models.IntegerField(default=1, editable=True)
    follows = models.ManyToManyField(AUTH_USER_MODEL, blank=True, related_name='poll_follows')

    def __str__(self):
        return self.title

    def delete(self, using=None, keep_parents=False):
        default_storage.delete("{}".format(self.photo))
        super().delete()

    def num_entries(self):
        return self.entries.count()

    def num_voters(self):
        num_owners = PollVote.objects.filter(poll__id=self.id).values('owner').distinct().count()
        return num_owners

    def num_votes(self):
        num_votes = PollVote.objects.filter(poll__id=self.id).count()
        return num_votes

    def num_comments(self):
        comments = PollComment.objects.filter(poll__id=self.id)
        return comments.count()

    class Meta:
        verbose_name_plural = "polls"


class PollComment(AbstractComment):
    poll = models.ForeignKey(Poll, related_name='poll_comments', on_delete=models.CASCADE, null=True)


class PollEntry(TimestampModerated):
    id = models.UUIDField(primary_key=True, db_index=True, editable=False, default=uuid_lib.uuid4)
    poll = models.ForeignKey(Poll, related_name='entries', on_delete=models.CASCADE)
    owner = models.ForeignKey(AUTH_USER_MODEL, on_delete=models.CASCADE, db_index=True)
    title = models.CharField(max_length=80, blank=False)
    slug = models.SlugField(blank=True)
    description = models.TextField(max_length=255, verbose_name='description of poll entry', blank=True, null=False)
    photo = VersatileImageField(upload_to=user_directory_path, height_field="height", width_field="width", blank=True)
    height = models.IntegerField(blank=True)
    width = models.IntegerField(blank=True)
    tags = TaggableManager(blank=True, through=UUIDTaggedItem)
    hash = models.CharField(max_length=64, null=True)
    size = models.BigIntegerField('size of file in bytes', blank=True, null=True)
    reports = models.ManyToManyField(AUTH_USER_MODEL, blank=True, related_name='entry_reports')

    class Meta:
        verbose_name_plural = "poll_entries"
        unique_together = ('hash', 'poll')

    def __str__(self):
        return "{}/{}/{}".format(self.id, self.poll, self.title)

    def delete(self, using=None, keep_parents=False):
        default_storage.delete("{}".format(self.photo))
        super().delete()

    def wins(self):
        votes = PollVote.objects.filter(winner__id=self.id).count()
        return votes

    def losses(self):
        votes = PollVote.objects.filter(loser__id=self.id).count()
        return votes

    def rating(self):
        return PollEntryRating.objects.get(entry=self).rating


class PollVote(TimestampModerated):
    id = models.UUIDField(db_index=True, default=uuid_lib.uuid4, editable=False, primary_key=True)
    owner = models.ForeignKey(AUTH_USER_MODEL, related_name='poll_vote_owner')
    poll = models.ForeignKey(Poll, related_name='poll_vote_polls', on_delete=models.CASCADE)
    combination = models.PositiveIntegerField()
    winner = models.ForeignKey(PollEntry, related_name='poll_vote_winners', blank=True, null=True)
    loser = models.ForeignKey(PollEntry, related_name='poll_vote_losers', blank=True, null=True)
    draw = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = "votes"
        unique_together = ('poll', 'owner', 'combination')


class PollEntryRating(TimestampModerated):
    id = models.UUIDField(default=uuid_lib.uuid4, editable=False, primary_key=True)
    entry = models.ForeignKey(PollEntry, related_name='entry_ratings')
    rating = models.DecimalField(decimal_places=2, max_digits=8, null=True, blank=True, default=1400)
