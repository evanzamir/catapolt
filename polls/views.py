import itertools
from datetime import datetime
import random
import time

from django.utils import timezone
from django.shortcuts import render
from django.db.models import Count, F, ExpressionWrapper, DecimalField, Value, Sum
from django.db.utils import DataError
from django.contrib.postgres.search import SearchQuery, SearchRank, SearchVector

from rest_framework import permissions, viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.decorators import list_route, detail_route, permission_classes
from rest_framework.response import Response
from rest_framework.exceptions import NotFound, PermissionDenied

from polls.models import PollComment
from polls.serializers import PollCommentSerializer
from polls.models import Poll, PollEntry, PollVote
from polls.permissions import IsOwnerOrReadOnly
from polls.serializers import PollSerializer, PollEntrySerializer, \
    PollVoteSerializer


class PollViewSet(viewsets.ModelViewSet):
    """
    This viewsetomatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """
    queryset = Poll.objects.all()
    serializer_class = PollSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsOwnerOrReadOnly, )
    parser_classes = (MultiPartParser, FormParser)

    def perform_create(self, serializer):
        """

        :param serializer:
        :return:
        """
        serializer.save(owner=self.request.user,
                        follows=[])

    @detail_route(methods=['post'])
    def close(self, request, pk):
        """

        :param request:
        :return:
        """
        poll = Poll.objects.get(id=pk)
        poll.expiration_date = timezone.now()
        poll.save()
        serializer = self.get_serializer(poll)
        return Response(
            data=serializer.data
        )

    @detail_route(methods=['post'])
    def follow(self, request, pk):
        """

        :param request:
        :return:
        """
        poll = Poll.objects.get(id=pk)
        if poll.owner.id != self.request.user.id:
            if self.request.user in poll.follows.all():
                poll.follows.remove(self.request.user)
            else:
                poll.follows.add(self.request.user)
            serializer = self.get_serializer(poll)
            return Response(
                data=serializer.data
            )
        else:
            raise PermissionDenied(detail="Poll owner not allowed to bookmark own poll.")

    @list_route(methods=['get'])
    def active(self, request):
        search_query_terms = request.query_params.get('searchQuery').split(' ')
        search_vector = SearchVector('entries__tags__name') + \
                        SearchVector('title') + \
                        SearchVector('description') + \
                        SearchVector('tags__name') + \
                        SearchVector('entries__title') + \
                        SearchVector('entries__description') + \
                        SearchVector('owner__username')
        search_query = SearchQuery(search_query_terms[0])
        for term in search_query_terms[1:]:
            search_query = SearchQuery(term) | search_query
        ids = Poll.objects.filter(poll_type=Poll.POLL,
                                  private=False,
                                  expiration_date__gt=timezone.now()) \
            .annotate(rank=SearchRank(search_vector, search_query)) \
            .order_by('-rank') \
            .filter(rank__gt=0) \
            .values_list('id')
        polls = Poll.objects.filter(id__in=ids)
        serializer = self.get_serializer(polls, many=True)
        return Response(
            data=serializer.data
        )

    @list_route(methods=['get'])
    def closed(self, request):
        search_query_terms = request.query_params.get('searchQuery').split(' ')
        search_vector = SearchVector('entries__tags__name') + \
                        SearchVector('title') + \
                        SearchVector('description') + \
                        SearchVector('tags__name') + \
                        SearchVector('entries__title') + \
                        SearchVector('entries__description') + \
                        SearchVector('owner__username')
        search_query = SearchQuery(search_query_terms[0])
        for term in search_query_terms[1:]:
            search_query = SearchQuery(term) | search_query
        ids = Poll.objects.filter(poll_type=Poll.POLL,
                                  private=False,
                                  expiration_date__lt=timezone.now()) \
            .annotate(rank=SearchRank(search_vector, search_query)) \
            .order_by('-rank') \
            .filter(rank__gt=0) \
            .values_list('id')
        polls = Poll.objects.filter(id__in=ids)
        serializer = self.get_serializer(polls, many=True)
        return Response(
            data=serializer.data
        )

    @list_route(methods=['get'])
    def all_active(self, request):
        polls = Poll.objects.filter(private=False, expiration_date__gt=timezone.now())
        serializer = self.get_serializer(polls, many=True)
        return Response(
            data=serializer.data
        )

    @list_route(methods=['get'])
    def all_closed(self, request):
        polls = Poll.objects.filter(private=False, expiration_date__lt=timezone.now())
        serializer = self.get_serializer(polls, many=True)
        return Response(
            data=serializer.data
        )

    @detail_route(methods=['get'])
    def choice(self, request, pk):
        try:
            entries = PollEntry.objects.filter(poll__id=pk).order_by('id')
            combos = list(itertools.combinations(entries, 2))
            choices = set(list(range(1, len(combos) + 1)))
            seen = set(PollVote.objects.filter(poll__id=pk,
                                               owner=request.user).values_list('combination', flat=True))
            random_unseen_choice = random.choice(list(choices - seen))
            combo = combos[random_unseen_choice - 1]
            choice = {
                "left": PollEntrySerializer(combo[0]).data,
                "right": PollEntrySerializer(combo[1]).data,
                "combo": random_unseen_choice
            }
            return Response(
                data=choice
            )
        except IndexError as e:
            raise NotFound(detail='Exhausted all available poll choices.')

    @detail_route(methods=['get'])
    def sample(self, request, pk):
        try:
            entries = PollEntry.objects.filter(poll__id=pk).order_by('id')
            combos = list(itertools.combinations(entries, 2))
            combo = combos[0]
            choice = {
                "left": PollEntrySerializer(combo[0]).data,
                "right": PollEntrySerializer(combo[1]).data,
                "combo": 0
            }
            return Response(
                data=choice
            )
        except IndexError as e:
            raise NotFound(detail='Exhausted all available poll choices.')

    @detail_route(methods=['get'])
    def entries(self, request, pk):
        """

        :param request:
        :param pk:
        :return:
        """
        poll = Poll.objects.get(id=pk)
        try:
            if poll.start_date:
                entries = PollEntry.objects.filter(poll__id=pk).annotate(rating=Sum('entry_ratings__rating')).order_by('-rating')
            else:
                entries = PollEntry.objects.filter(poll__id=pk).order_by('-created')
            return Response(
                data=PollEntrySerializer(entries, many=True).data
            )
        except DataError as e:
            entries = PollEntry.objects.filter(poll__id=pk).order_by('-created')
            return Response(
                data=PollEntrySerializer(entries, many=True).data
            )

    @detail_route(methods=['post'])
    def activate(self, request, pk):
        """

        :param request:
        :param pk:
        :return:
        """
        poll = Poll.objects.get(id=pk)
        self.check_object_permissions(self.request, poll)
        poll.start_date = timezone.now()
        poll.expiration_date = timezone.now() + timezone.timedelta(days=7)
        poll.save()
        return Response(
            data=PollSerializer(poll).data
        )

    @detail_route(methods=['get'])
    def comments(self, request, pk):
        """

        :param request:
        :param pk:
        :return:
        """
        comments = PollComment.objects.filter(poll__id=pk).order_by('created')
        return Response(
            data=PollCommentSerializer(comments, many=True).data
        )


class PollCommentViewSet(viewsets.ModelViewSet):
    """
    This viewsetomatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    """
    queryset = PollComment.objects.all()
    serializer_class = PollCommentSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsOwnerOrReadOnly,)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)


class PollEntryViewSet(viewsets.ModelViewSet):
    queryset = PollEntry.objects.all()
    serializer_class = PollEntrySerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsOwnerOrReadOnly, permissions.AllowAny)
    parser_classes = (MultiPartParser, FormParser)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class PollVoteViewSet(viewsets.ModelViewSet):
    queryset = PollVote.objects.all()
    serializer_class = PollVoteSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsOwnerOrReadOnly, permissions.AllowAny)
    parser_classes = (MultiPartParser, FormParser)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


def index(request):
    return render(request, 'index.html')
