from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from polls import views

router = DefaultRouter()
router.register(r'polls', views.PollViewSet)
router.register(r'entries', views.PollEntryViewSet)
router.register(r'votes', views.PollVoteViewSet)
router.register(r'comments', views.PollCommentViewSet)


urlpatterns = [
    url(r'^', include(router.urls)),
]
