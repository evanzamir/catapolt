from django.contrib import admin

from polls.models import Poll, PollEntry, PollVote, PollComment, PollEntryRating


# Register your models here.

class PollAdmin(admin.ModelAdmin):
    model = Poll
    list_display = ['id', 'owner', 'title', 'slug', 'description', 'created', 'updated', 'expiration_date', 'tag_list',
                    'start_date', 'photo', 'private', 'users_threshold', 'max_entries_per_user',
                    'poll_type', 'height', 'width']

    def tag_list(self, obj):
        return u", ".join(o.name for o in obj.tags.all())


class PollEntryAdmin(admin.ModelAdmin):
    model = PollEntry
    list_display = ['id', 'owner', 'photo', 'poll', 'created', 'updated', 'moderation_code',
                    'title', 'description', 'tag_list']

    def tag_list(self, obj):
        return u", ".join(o.name for o in obj.tags.all())


class PollVoteAdmin(admin.ModelAdmin):
    model = PollVote
    list_display = ['id', 'poll', 'owner', 'combination', 'winner', 'loser']


class PollCommentAdmin(admin.ModelAdmin):
    model = PollComment
    list_display = ['text', 'poll', 'owner', 'created', 'updated']


class PollEntryRatingAdmin(admin.ModelAdmin):
    model = PollEntryRating
    list_display = ['id', 'entry', 'rating']


admin.site.register(Poll, PollAdmin)
admin.site.register(PollEntry, PollEntryAdmin)
admin.site.register(PollVote, PollVoteAdmin)
admin.site.register(PollComment, PollCommentAdmin)
admin.site.register(PollEntryRating, PollEntryRatingAdmin)
