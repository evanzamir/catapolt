# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-12-22 03:21
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('polls', '0005_pollentryrating'),
    ]

    operations = [
        migrations.AddField(
            model_name='poll',
            name='follows',
            field=models.ManyToManyField(blank=True, related_name='poll_follows', to=settings.AUTH_USER_MODEL),
        ),
    ]
