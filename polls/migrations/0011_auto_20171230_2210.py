# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-12-30 22:10
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0010_auto_20171229_0340'),
    ]

    operations = [
        migrations.AddField(
            model_name='pollvote',
            name='draw',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='pollentryrating',
            name='rating',
            field=models.DecimalField(blank=True, decimal_places=2, default=1400, max_digits=8, null=True),
        ),
    ]
