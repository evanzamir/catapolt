import math
from decimal import Decimal
import time

from django.utils.text import slugify

from rest_framework import serializers
from rest_framework.serializers import ValidationError
from rest_framework.exceptions import PermissionDenied, APIException
from taggit_serializer.serializers import TagListSerializerField

from polls.models import Poll, PollEntry, PollVote, PollComment, PollEntryRating
from profiles.serializers import ProfileSerializer
from photos.serializers import Base64ImageField


class PollSerializer(serializers.ModelSerializer):
    photo = Base64ImageField(use_url=True)
    owner = ProfileSerializer(read_only=True)
    tags = TagListSerializerField()

    class Meta:
        model = Poll
        fields = ('id', 'created', 'title', 'description', 'moderation_code', 'owner', 'num_entries',
                  'updated', 'tags', 'expiration_date', 'slug', 'start_date', 'poll_type', 'num_voters',
                  'users_threshold', 'max_entries_per_user', 'photo', 'private', 'height', 'width', 'num_votes',
                  'follows', 'num_comments')

    def create(self, validated_data):
        title = validated_data.pop('title')
        poll = Poll.objects.create(title=title,
                                   slug=slugify(title),
                                   **validated_data)
        p = Poll.objects.get(id=poll.id)
        [p.tags.add(tag) for tag in validated_data['tags']]
        return poll


class PollEntrySerializer(serializers.ModelSerializer):
    owner = ProfileSerializer(read_only=True)
    photo = Base64ImageField(use_url=True)
    tags = TagListSerializerField()

    class Meta:
        model = PollEntry
        fields = ('id', 'created', 'moderation_code', 'updated', 'owner', 'poll',
                  'photo', 'wins', 'losses', 'rating',
                  'tags', 'height', 'width', 'slug', 'title', 'description', 'hash')

    def create(self, validated_data):
        poll = Poll.objects.get(id=validated_data['poll'].id)
        if poll.owner != validated_data['owner']:
            raise PermissionDenied('Only poll owner can submit entries to this poll.')
        else:
            entry = PollEntry.objects.create(slug=slugify(validated_data['title']),
                                             **validated_data)
            PollEntryRating.objects.create(entry=entry)
            e = PollEntry.objects.get(id=entry.id)
            [e.tags.add(tag) for tag in validated_data['tags']]
            return entry


class PollVoteSerializer(serializers.ModelSerializer):
    owner = ProfileSerializer(read_only=True)

    def create(self, validated_data):
        vote = PollVote.objects.create(**validated_data)
        winner = vote.winner
        loser = vote.loser
        winner_rtg = PollEntryRating.objects.get(entry__id=winner.id)
        loser_rtg = PollEntryRating.objects.get(entry__id=loser.id)
        winner_current_rating = math.pow(10, winner_rtg.rating / 400)
        loser_current_rating = math.pow(10, loser_rtg.rating / 400)
        winner_expected_score = winner_current_rating / (winner_current_rating + loser_current_rating)
        loser_expected_score = loser_current_rating / (winner_current_rating + loser_current_rating)
        if not vote.draw:
            winner_rtg.rating = winner_rtg.rating + Decimal(32*(1-winner_expected_score))
            loser_rtg.rating = loser_rtg.rating - Decimal(32*loser_expected_score)
        else:
            winner_rtg.rating = winner_rtg.rating + Decimal(32*(0.5-winner_expected_score))
            loser_rtg.rating = loser_rtg.rating + Decimal(32*(0.5-loser_expected_score))
        winner_rtg.save()
        loser_rtg.save()
        return vote

    class Meta:
        model = PollVote
        fields = ('id', 'poll', 'owner', 'combination', 'winner', 'loser', 'draw')


class PollCommentSerializer(serializers.ModelSerializer):
    owner = ProfileSerializer(read_only=True)

    class Meta:
        model = PollComment
        fields = ('id', 'created', 'moderation_code', 'updated', 'owner', 'text', 'poll')
