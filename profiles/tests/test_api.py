from django.contrib.auth import models
from rest_framework import test, status
from rest_framework.test import APIClient

from profiles.models import Profile


class ProfilesAPITest(test.APITestCase):
    """
    Testing django-rest-framework API
    """

    def setUp(self):
        self.admin, result = models.User.objects.get_or_create(username='evanzamir', password='mazda603')
        self.user1, result = models.User.objects.get_or_create(username='judah', password='judah2016')
        self.user2, result = models.User.objects.get_or_create(username='ellie', password='eliana2014')
        self.user3, result = models.User.objects.get_or_create(username='charlie', password='charlie2014')
        self.user4, result = models.User.objects.get_or_create(username='julian', password='julian2017')
        self.user5, result = models.User.objects.get_or_create(username='georgina', password='georgina2011')

    def test_create_profile(self):
        new_user = models.User(
            username='stephencurry',
            first_name='Stephen',
            last_name='Curry',
            email='stephen.curry@gsw.com',
            password='long3ptshots'
        )
        new_user.save()
        client = APIClient()
        client.force_authenticate(new_user)
        response = client.post(
            path='/api/profiles/',
            data={
                'bio': "world's best 3pt shooter and golfer"
            }
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        profile = Profile.objects.get(user=new_user)
        self.assertEqual(response.data['bio'], profile.bio)
        client.logout()

    def test_get_profile(self):
        profile, _ = Profile.objects.get_or_create(user=self.user1, bio="dabbling in analytics")

        client = APIClient()
        client.force_authenticate(self.user2)

        response = client.get(
            path='/api/profiles/{}/'.format(self.user1.username)
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['bio'], profile.bio)
        client.logout()

    def test_create_duplicate_profile(self):
        new_user = models.User(
            username='bobjones',
            password=';alsdfkj;asoi'
        )
        new_user.save()
        client = APIClient()
        client.force_authenticate(new_user)
        response = client.post(
            path='/api/profiles/'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        same_user = models.User.objects.get(
            username='bobjones'
        )
        response = client.post(
            path='/api/profiles/'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        client.logout()

    def test_get_own_profile(self):
        profile, _ = Profile.objects.get_or_create(user=self.user3, bio="dabbling in analytics")

        client = APIClient()
        client.force_authenticate(self.user3)

        response = client.get(
            path='/api/profiles/{}/'.format(self.user3.username)
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['user'], 'charlie')
        self.assertEqual(response.data['bio'], "dabbling in analytics")
        print(response.data)
        client.logout()
