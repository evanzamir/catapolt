import json

from django.contrib.auth.models import User
from django.shortcuts import render
from django.db.utils import IntegrityError
from django.utils import timezone

from rest_framework import permissions, viewsets, status, serializers
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication, TokenAuthentication, BasicAuthentication
from rest_framework.decorators import list_route, detail_route
from rest_framework.parsers import MultiPartParser, FormParser

from profiles.models import Profile
from photos.models import Photo
from profiles.permissions import IsOwnerOrReadOnly
from profiles.serializers import ProfileSerializer
from photos.serializers import PhotoSerializer
from polls.models import Poll
from polls.serializers import PollSerializer


class ProfileViewSet(viewsets.ModelViewSet):
    """
    This viewsetomatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    """
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    authentication_classes = (TokenAuthentication, BasicAuthentication)
    permission_classes = (IsOwnerOrReadOnly,)
    parser_classes = (MultiPartParser, FormParser)

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

    @detail_route(methods=['get'])
    def polls(self, request, pk):
        polls = Poll.objects.filter(owner__id=pk,
                                    poll_type=Poll.POLL).order_by('-created')
        page = self.paginate_queryset(polls)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = PollSerializer(polls, many=True)
        return Response(
            data=serializer.data
        )

    @detail_route(methods=['get'])
    def open(self, request, pk):
        polls = Poll.objects.filter(owner__id=pk,
                                    poll_type=Poll.POLL,
                                    expiration_date__gt=timezone.now()
                                    ).order_by('-created')
        page = self.paginate_queryset(polls)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = PollSerializer(polls, many=True)
        return Response(
            data=serializer.data
        )

    @detail_route(methods=['get'])
    def public_open(self, request, pk):
        polls = Poll.objects.filter(owner__id=pk,
                                    poll_type=Poll.POLL,
                                    private=False,
                                    expiration_date__gt=timezone.now()
                                    ).order_by('-created')
        page = self.paginate_queryset(polls)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = PollSerializer(polls, many=True)
        return Response(
            data=serializer.data
        )

    @detail_route(methods=['get'])
    def closed(self, request, pk):
        polls = Poll.objects.filter(owner__id=pk,
                                    poll_type=Poll.POLL,
                                    expiration_date__lt=timezone.now()
                                    ).order_by('-created')
        page = self.paginate_queryset(polls)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = PollSerializer(polls, many=True)
        return Response(
            data=serializer.data
        )

    @detail_route(methods=['get'])
    def public_closed(self, request, pk):
        polls = Poll.objects.filter(owner__id=pk,
                                    poll_type=Poll.POLL,
                                    private=False,
                                    expiration_date__lt=timezone.now()
                                    ).order_by('-created')
        page = self.paginate_queryset(polls)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = PollSerializer(polls, many=True)
        return Response(
            data=serializer.data
        )

    @detail_route(methods=['get'])
    def unactivated(self, request, pk):
        polls = Poll.objects.filter(owner__id=pk,
                                    poll_type=Poll.POLL,
                                    start_date__isnull=True
                                    ).order_by('-created')
        page = self.paginate_queryset(polls)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = PollSerializer(polls, many=True)
        return Response(
            data=serializer.data
        )

    @detail_route(methods=['get'])
    def saved(self, request, pk):
        profile = Profile.objects.get(id=pk)
        polls = profile.poll_follows.all()
        serializer = PollSerializer(polls, many=True)
        return Response(
            data=serializer.data
        )

    @detail_route(methods=['get'])
    def saved_public(self, request, pk):
        profile = Profile.objects.get(id=pk)
        polls = profile.poll_follows.filter(private=False).order_by('-created')
        serializer = PollSerializer(polls, many=True)
        return Response(
            data=serializer.data
        )
