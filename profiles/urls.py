from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from profiles import views

router = DefaultRouter()
router.register(r'profiles', views.ProfileViewSet)

app_name = 'profiles'
urlpatterns = [
    url(r'^', include(router.urls)),
]
