from uuid import uuid4

from django.db import models
from django.contrib.auth.models import User, AbstractUser

from scouting.core import TimestampModerated


# Create your models here.
class Profile(TimestampModerated, AbstractUser):
    id = models.UUIDField(primary_key=True, db_index=True, default=uuid4, editable=False)
    bio = models.CharField(max_length=255, blank=True, default='')
    media_url = models.URLField(blank=True, default='')
    first_name = models.CharField(max_length=30, blank=True, default='')
    last_name = models.CharField(max_length=30, blank=True, default='')

    class Meta:
        verbose_name_plural = "profiles"


