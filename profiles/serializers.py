from rest_framework import serializers

from profiles.models import Profile


class ProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = Profile
        fields = ('id', 'created', 'moderation_code', 'updated', 'bio',
                  'first_name', 'last_name', 'last_login', 'media_url', 'date_joined',
                  'username')
