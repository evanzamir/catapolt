# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2018-01-08 23:16
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='credits',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='votes_to_unlock_credit',
        ),
    ]
