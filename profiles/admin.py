from django.contrib import admin

from profiles.models import Profile


# Register your models here.

class ProfileAdmin(admin.ModelAdmin):
    model = Profile
    list_display = ['id', 'created', 'moderation_code', 'updated', 'bio', 'first_name', 'last_name']


admin.site.register(Profile, ProfileAdmin)
